from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import jadwal, tambah, delete, detail
from .models import Schedule
from .forms import Schedule_Form
# Create your tests here.

class Testing(TestCase):
    def test_url_jadwal(self):
        response = Client().get('/jadwal/')
        self.assertEqual(response.status_code, 200)
    def test_url_tambah_jadwal(self):
        response = Client().get('/jadwal/tambah/')
        self.assertEqual(response.status_code, 200)
    def test_url_detail(self):
        jadwal = Schedule.objects.create(matkul="pepewe",dosen="bu dosen",sks=3,deskripsi="deskripsi ppw",tahun="2020/2021",semester="Ganjil",ruang="zoom",jadwal="jadwal1")
        idjadwal = jadwal.id
        response = Client().get('/jadwal/detail/' + str(idjadwal))
        self.assertEqual(response.status_code, 200)
    def test_url_delete(self):
        jadwal = Schedule.objects.create(matkul="pepewe",dosen="bu dosen",sks=3,deskripsi="deskripsi ppw",tahun="2020/2021",semester="Ganjil",ruang="zoom",jadwal="jadwal1")
        idjadwal = jadwal.id
        response = Client().get('/jadwal/delete/' + str(idjadwal))
        self.assertEqual(response.status_code, 302)
    def test_post_jadwal(self):
        response = self.client.post('/jadwal/tambah/', data={"matkul":"pepewe","dosen":"bu dosen","sks":3,"deskripsi":"deskripsi ppw","tahun":"2020/2021","semester":"Ganjil","ruang":"zoom","jadwal":"jadwal1"})
        self.assertEqual(response.status_code, 302)
    def test_template_jadwal(self):
        response = Client().get('/jadwal/')
        self.assertTemplateUsed(response, 'jadwal/jadwal.html')
    def test_template_tambah(self):
        response = Client().get('/jadwal/tambah/')
        self.assertTemplateUsed(response, 'jadwal/tambah.html')
    def test_model(self):
        jadwal = Schedule.objects.create(matkul="pepewe",dosen="bu dosen",sks=3,deskripsi="deskripsi ppw",tahun="2020/2021",semester="Ganjil",ruang="zoom",jadwal="jadwal1")
        counting_all_available_todo = Schedule.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    def test_delete_jadwal(self):
        jadwal = Schedule.objects.create(matkul="pepewe",dosen="bu dosen",sks=3,deskripsi="deskripsi ppw",tahun="2020/2021",semester="Ganjil",ruang="zoom",jadwal="jadwal1")
        jadwal.delete()
        self.assertEqual(Schedule.objects.all().count(), 0)