from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Schedule(models.Model):
    matkul = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    sks = models.PositiveIntegerField(default = 1)
    deskripsi = models.TextField()
    list_tahun = {
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023'),
        ('2023/2024', '2023/2024'),
    }
    tahun = models.CharField(
        max_length=100,
        choices = list_tahun,
        default = '2020/2021',
    )
    list_semester = {
        ('Ganjil', 'Ganjil'),
        ('Genap', 'Genap'),
    }
    semester = models.CharField(
        max_length=100,
        choices = list_semester,
        default = ('Ganjil', 'Ganjil'),
    )
    ruang = models.CharField(max_length = 100)
    jadwal = models.CharField(max_length=100)