# Generated by Django 3.1.2 on 2020-10-17 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul', models.CharField(max_length=100)),
                ('dosen', models.CharField(max_length=100)),
                ('sks', models.PositiveIntegerField(default=1)),
                ('deskripsi', models.TextField()),
                ('tahun', models.CharField(choices=[('2022/2023', '2022/2023'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2023/2024', '2023/2024')], default='2020/2021', max_length=100)),
                ('semester', models.CharField(choices=[('Ganjil', 'Ganjil'), ('Genap', 'Genap')], default=('Ganjil', 'Ganjil'), max_length=100)),
                ('ruang', models.CharField(max_length=100)),
                ('jadwal', models.CharField(max_length=100)),
            ],
        ),
    ]
