# Generated by Django 3.1.2 on 2020-10-24 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0022_auto_20201023_2311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='semester',
            field=models.CharField(choices=[('Genap', 'Genap'), ('Ganjil', 'Ganjil')], default=('Ganjil', 'Ganjil'), max_length=100),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='tahun',
            field=models.CharField(choices=[('2023/2024', '2023/2024'), ('2022/2023', '2022/2023'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022')], default='2020/2021', max_length=100),
        ),
    ]
