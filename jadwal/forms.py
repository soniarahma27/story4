from django import forms
from .models import Schedule

class Schedule_Form(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = "__all__"
        labels = {
            'matkul' : 'Mata Kuliah',
            'dosen' : 'Dosen Pengajar',
            'sks' : 'SKS',
            'deskripsi' : 'Deskripsi',
            'tahun' : 'Tahun',
            'semester' : 'semester',
            'ruang' : 'Ruang',
            'jadwal' : 'Jadwal',
        }
