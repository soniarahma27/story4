from django.shortcuts import render, redirect
from .forms import Schedule_Form
from .models import Schedule

# Create your views here.

def jadwal(request):
    schedule = Schedule.objects.all()

    context = {
        'all_schedule':schedule
    }
    return render(request, 'jadwal/jadwal.html', context)

def tambah(request):
    schedule_form = Schedule_Form(request.POST or None)
    if request.method == "POST":
        if schedule_form.is_valid():
            schedule_form.save()
            return redirect('jadwal:jadwal')
    context = {
        'schedule_form' : schedule_form
    }
    return render(request, 'jadwal/tambah.html', context)

def delete(request, delete_id):
    Schedule.objects.filter(id = delete_id).delete()
    return redirect('jadwal:jadwal')

def detail(request, detail_id):
    jenis = Schedule.objects.get(pk = detail_id)
    context = {'jenis': jenis}
    return render(request, 'jadwal/detail.html', context)