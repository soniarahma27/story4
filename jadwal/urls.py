from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('tambah/', views.tambah, name='tambah'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
    path('detail/<int:detail_id>', views.detail, name='detail'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
