from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import story7
# Create your tests here.

class story7Test(TestCase):
    def test_story7_url_exists(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
    def test_views(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)
    def test_story7_using_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/story7.html')