from django.shortcuts import render

# Create your views here.
def story7(request):
    context = {
        'contents': [
            ('Aktivitas saat ini', ['Menjalani kehidupan sebagai mahasiswa Fasilkom UI semester 3']),
            ('Pengalaman Kepanitiaan', ['Staf Software Engineering Academy Compfest 12', 'Staf Simulasi SBM KGTK IMAMI UI 17']),
            ('Pengalaman Organisasi', ['FUKI Fasilkom UI 2020', 'KIR (Kelompok Ilmiah Remaja)']),
            ('Prestasi', ['Juara 3 OSK Matematika SMA 2018', 'Juara 3 Lomba Matematika SMA di Universitas Bung Hatta 2017', 'Juara 1 Lomba Matematika SMA di STKIP YDB 2017']),
        ]
    }
    return render(request, 'story7/story7.html', context)