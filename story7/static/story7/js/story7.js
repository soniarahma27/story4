 function change(id1, id2) {
     var content1 = document.getElementById(id1);
     var content2 = document.getElementById(id2);
     var temp = content1.innerHTML;
     content1.innerHTML = content2.innerHTML;
     content2.innerHTML = temp;
 }

 function changeup(ch) {
     if (ch == 1) return;
     var up = ch - 1;
     change(`title-${up}`, `title-${ch}`);
     change(`content-${up}`, `content-${ch}`);
 }

 function changedown(ch) {
     if (ch == 4) return;
     var down = ch + 1;
     change(`title-${down}`, `title-${ch}`);
     change(`content-${down}`, `content-${ch}`);
 }

 $(".accordion-btn").click(function() {
     var bodyId = $(this).attr('data-body');
     $(`#${bodyId}`).slideToggle("slow");
 });