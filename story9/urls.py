from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.masuk, name='masuk'),
    path('signup/', views.daftar, name='daftar'),
    path('logout/', views.keluar, name='keluar'),
]