from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.test import TestCase, SimpleTestCase, Client, LiveServerTestCase
from .views import index, masuk, keluar, daftar
from django.urls import resolve
# Create your tests here.
class Maintest(TestCase):
    def test_url_home(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)
    def test_url_login(self):
        response = self.client.get('/story9/login/')
        self.assertEqual(response.status_code, 200)
    def test_url_signup(self):
        response = self.client.get('/story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')
    def test_template_login(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'story9/login.html')
    def test_template_signup(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'story9/signup.html')

    def test_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)
    def test_register_func(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, daftar)
    def test_login_func(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, masuk)
    def test_logout_func(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, keluar)

    def test_new_user_created_and_redirected(self):
        data = {'username':'user01', 'password1':'katasandi01', 'password2':'katasandi01'}
        response = self.client.post('/story9/signup/', data=data)
        self.assertEqual(response.status_code, 302)

    def test_user_login_success_and_redirected(self):
        user = User.objects.create_user(username="user01", password="katasandi01")
        data = {'username':'user01', 'password':'katasandi01'}
        response = self.client.post('/story9/login/', data)
        self.assertRedirects(response, '/story9/')

    def test_logout(self):
        self.client = Client()
        self.client.login(username='user01', password='katasandi01')
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

class Logintest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user01', password='katasandi01')
        self.user.save()
    def tearDown(self):
        self.user.delete()
    def test_login_correct_username_password(self):
        user = authenticate(username='user01', password='katasandi01')
        self.assertTrue((user is not None) and user.is_authenticated)
    def test_wrong_username(self):
        user = authenticate(username='wrong', password='katasandi01')
        self.assertFalse(user is not None and user.is_authenticated)
    def test_wrong_pssword(self):
        user = authenticate(username='user01', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)
    def test_message_invalid_username_password_shown(self):
        data = {'username':'test01', 'password':'testtest01'}
        response = self.client.post('/story9/login/', data)
        response_dec = response.content.decode('utf-8')
        self.assertIn('Username/Password yang anda masukkan salah', response_dec)
