from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def index(request):
    return render(request, 'story9/index.html')

def masuk(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('story9:index')
        else:
            return render(request, 'story9/login.html', {'response':'Username/Password yang anda masukkan salah'})
    return render(request, 'story9/login.html')

def daftar(request):
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story9:masuk')
    return render(request, 'story9/signup.html', {'form' : form})

def keluar(request):
    request.session.flush()
    logout(request)
    return redirect('story9:index')
