from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('index/', views.home, name='index'),
    path('story1/', views.story1, name='story1'),
    path('pendidikan/', views.pendidikan, name='pendidikan'),
]
