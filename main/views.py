from django.shortcuts import render


def home(request):
    return render(request, 'main/index.html')

def pendidikan(request):
    return render(request, 'main/pendidikan.html')

def story1(request):
    return render(request, 'main/story1.html')