from django.urls import path
from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('tambah/', views.tambah, name='tambah'),
    path('peserta/<int:index>/', views.peserta, name='peserta'),
    path('deleteKegiatan/<int:delete_id>', views.deleteKegiatan, name='deleteKegiatan'),
    path('deletePeserta/<int:delete_id>', views.deletePeserta, name='deletePeserta'),
    path('detail/<int:detail_id>/', views.detail, name='detail'),
]