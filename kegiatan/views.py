from django.shortcuts import render, redirect
from .forms import Kegiatan_Form, Peserta_Form
from .models import Kegiatan, Peserta
# Create your views here.

def kegiatan(request):
    all_activity = Kegiatan.objects.all()
    context = {
        'all_activity':all_activity
    }
    return render(request, 'kegiatan/kegiatan.html', context)

def tambah(request):
    kegiatan_form = Kegiatan_Form(request.POST or None)
    if request.method == "POST":
        if kegiatan_form.is_valid():
            kegiatan_form.save()
        return redirect('kegiatan:kegiatan')
    
    context = {
        'kegiatan_form' : kegiatan_form
    }
    return render(request, 'kegiatan/tambah.html', context)

def peserta(request, index):
    peserta_form = Peserta_Form(request.POST or None)
    if request.method == "POST":
        if peserta_form.is_valid():
            peserta = Peserta(kegiatan=Kegiatan.objects.get(pk = index), nama=peserta_form.data['nama'])
            peserta.save()
        x = '/kegiatan/detail/' + str(index)
        return redirect(x)
    
    context = {
        'peserta_form' : peserta_form
    }
    return render(request, 'kegiatan/peserta.html', context)

def deleteKegiatan(request, delete_id):
    Kegiatan.objects.filter(id = delete_id).delete()
    return redirect('kegiatan:kegiatan')

def deletePeserta(request, delete_id):
    p = Peserta.objects.get(pk = delete_id)
    p.delete()
    x = '/kegiatan/detail/' + str(p.kegiatan.id)
    return redirect(x)

def detail(request, detail_id):
    jenis = Kegiatan.objects.get(pk = detail_id)
    semua_peserta = Peserta.objects.filter(kegiatan = jenis)
    return render(request, 'kegiatan/detail.html', {'jenis': jenis, 'semua_peserta':semua_peserta})
