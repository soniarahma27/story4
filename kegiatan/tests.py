from django.test import TestCase, Client
from django.urls import resolve
from .views import kegiatan, tambah, peserta, detail, deleteKegiatan, deletePeserta
from .models import Kegiatan, Peserta
from .forms import Kegiatan_Form, Peserta_Form
from .apps import KegiatanConfig

# Create your tests here.
class Testing(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)
    def test_event2_url_is_exist(self):
        response = Client().get('/kegiatan/tambah/')
        self.assertEqual(response.status_code, 200)
    def test_event3_url_is_exist(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        response = Client().get('/kegiatan/detail/' + str(idkegiatan) + "/")
        self.assertEqual(response.status_code, 200)
    def test_event4_url_is_exist(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        response = Client().get('/kegiatan/peserta/' + str(idkegiatan) + "/")
        self.assertEqual(response.status_code, 200)
    def test_event5_url_is_exist(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen", tanggal="2020-12-12")
        id = kegiatan.id
        response = Client().get('/kegiatan/deleteKegiatan/' + str(id))
        self.assertEqual(response.status_code, 302)
    def test_event6_url_is_exist(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen", tanggal="2020-12-12")
        peserta = Peserta.objects.create(nama="Sonia", kegiatan=kegiatan)
        id = peserta.id
        response = Client().get('/kegiatan/deletePeserta/' + str(id))
        self.assertEqual(response.status_code, 302)

    def test_event_using_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')
    def test_event2_using_template(self):
        response = Client().get('/kegiatan/tambah/')
        self.assertTemplateUsed(response, 'kegiatan/tambah.html')
    def test_event3_using_template(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        response = Client().get('/kegiatan/detail/' + str(idkegiatan) + "/")
        self.assertTemplateUsed(response, 'kegiatan/detail.html')
    def test_event4_using_template(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        response = Client().get('/kegiatan/peserta/' + str(idkegiatan) + "/")
        self.assertTemplateUsed(response, 'kegiatan/peserta.html')
    def test_post_tambah_success(self):
        response = self.client.post('/kegiatan/tambah/', data={"nama_kegiatan": "Makan", "deskripsi": "Makan ramen", "tanggal":"2020-12-12"})
        self.assertEqual(response.status_code, 302)
    def test_post_peserta_success(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        response = self.client.post('/kegiatan/peserta/' + str(idkegiatan) + "/", data={"nama": "Sonia"})
        self.assertEqual(response.status_code, 302)

class TestFunc(TestCase):
    def test_views1_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)
    def test_event_func(self):
        found = resolve('/kegiatan/tambah/')
        self.assertEqual(found.func, tambah)
    def test_views3_func(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        found = resolve('/kegiatan/detail/' + str(idkegiatan) + "/")
        self.assertEqual(found.func, detail)
    def test_views4_func(self):
        namakegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        idkegiatan = namakegiatan.id
        found = resolve('/kegiatan/peserta/' + str(idkegiatan) +"/")
        self.assertEqual(found.func, peserta)

class TestModels(TestCase):
    def test_model_can_create(self):
        new_activity = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        counting_all_available_todo = Kegiatan.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    def test_model2_can_create(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        peserta = Peserta.objects.create(nama="Sonia", kegiatan=activity)
        counting_all_available_todo = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    def test_model_can_print(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        self.assertEqual(kegiatan.__str__(), "Makan")
    def test_model2_can_print(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        peserta = Peserta.objects.create(nama="Sonia", kegiatan=activity)
        self.assertEqual(peserta.__str__(), "Sonia")

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(KegiatanConfig.name, "kegiatan")

class TestDelete(TestCase):
    def test_deleteKegiatan(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        activity.delete()
        self.assertEqual(Kegiatan.objects.all().count(), 0)
    def test_deletePeserta(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Makan", deskripsi="Makan ramen",tanggal="2020-12-12")
        peserta = Peserta.objects.create(nama="Sonia", kegiatan=activity)
        peserta.delete()
        self.assertEqual(Peserta.objects.all().count(), 0)
