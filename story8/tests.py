from django.test import TestCase
import json


# Create your tests here.
class Testing(TestCase):
	def test_url(self):
		response = self.client.get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_buku(self):
		response = self.client.get('/story8/data/?q=naruto')
		response_json = json.loads(response.content.decode())
		self.assertEqual(response.status_code, 200)
		self.assertNotEquals(response_json, {'totalItems': 0})
