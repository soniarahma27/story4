function search() {
    var key = $("#keyword").val();
    $.ajax({
        url: '/story8/data?q=' + key,
        success: function(data) {
            var array_items = data.items;
            console.log(array_items);
            $("tbody").empty();
            for (i = 0; i < array_items.length; i++) {
                var judul = array_items[i].volumeInfo.title;
                var penulis = array_items[i].volumeInfo.authors ? array_items[i].volumeInfo.authors : '-';
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail == undefined ? '-' : array_items[i].volumeInfo.imageLinks.smallThumbnail;
                //console.log(gambar);
                var link = array_items[i].volumeInfo.infoLink;
                $("tbody").append("<tr>" + "<td>" + (i + 1) + "</td>" + "<td><a href=" + link + ">" + judul + "</a></td>" + "<td>" + penulis + "</td>" + "<td><img src=" + gambar + "></td>" + "</tr>")
            }
        }
    })
}
$("#search").click(search);
$("#keyword").keyup(function(event) {
    if (event.keyCode == 13)
        search();

})