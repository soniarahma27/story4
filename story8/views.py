from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
import requests
import json
# Create your views here.
def story8(request):
    return render(request, 'story8/story8.html')
def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)